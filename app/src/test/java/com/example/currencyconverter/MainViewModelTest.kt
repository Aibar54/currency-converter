package com.example.currencyconverter

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.currencyconverter.domain.model.Currency
import com.example.currencyconverter.domain.repository.CurrencyRepository
import com.example.currencyconverter.presentation.MainViewModel
import com.example.currencyconverter.util.Resource
import com.example.currencyconverter.util.round
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockRepository: CurrencyRepository

    private lateinit var viewModel: MainViewModel

    private val delta = 0.001

    @Before
    fun setUp() {
        viewModel = MainViewModel(mockRepository)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @Test
    fun `test convert() success`(): Unit = runBlocking {
        val amount = "10"
        val base = "USD"

        val rates = listOf(
            Currency("EUR", 0.8, -0.1),
            Currency("GBP", 0.7, 0.2),
            Currency("JPY", 110.0, 0.0)
        )

        `when`(mockRepository.getRates(base)).thenReturn(Resource.Success(rates))

        var loadingWasTriggered = false
        viewModel.conversion.observe(TestLifecycleOwner()) {
            when (it) {
                MainViewModel.Event.Loading -> loadingWasTriggered = true
                is MainViewModel.Event.Error -> {
                    assert(false)
                }
                is MainViewModel.Event.Success -> {
                    assertTrue(loadingWasTriggered)
                    val convertedCurrencies = it.data

                    assertEquals(convertedCurrencies.size, rates.size)

                    convertedCurrencies.forEachIndexed { index, currency ->
                        val expectedConvertedAmount = (rates[index].rate * amount.toDouble()).round(2)
                        assertEquals(currency.name, rates[index].name)
                        assertEquals(currency.rate, expectedConvertedAmount, delta)
                        assertEquals(currency.changeForLast24Hour, rates[index].changeForLast24Hour, delta)
                    }
                }
            }
        }

        viewModel.convert(amount, base)
        Mockito.verify(mockRepository).getRates(base)
    }

    @Test
    fun `test convert() error`(): Unit = runBlocking {
        val amount = "10"
        val base = "USD"
        val message = "msg"

        `when`(mockRepository.getRates(base)).thenReturn(Resource.Error(message))

        var loadingWasTriggered = false
        viewModel.conversion.observe(TestLifecycleOwner()) {
            when (it) {
                MainViewModel.Event.Loading -> loadingWasTriggered = true
                is MainViewModel.Event.Error -> {
                    assertTrue(loadingWasTriggered)
                    assertEquals(it.message, message)
                }
                is MainViewModel.Event.Success -> {
                    assert(false)
                }
            }
        }

        viewModel.convert(amount, base)
        Mockito.verify(mockRepository).getRates(base)
    }
}