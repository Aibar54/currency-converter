package com.example.currencyconverter.presentation.fragment

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.currencyconverter.R
import com.example.currencyconverter.data.local.TokenManager
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class LoginFragmentTest {

    private lateinit var navController: NavController
    private lateinit var loginScenario: FragmentScenario<LoginFragment>
    private val tokenManager = TokenManager.getInstance()

    @Test
    fun login_user_has_no_token_case() {
        tokenManager.clearToken()

        setUpFragment()

        assertFalse(tokenManager.isTokenValid())

        onView(withId(R.id.loginButton)).perform(click())

        assertTrue(tokenManager.isTokenValid())

        assertEquals(navController.currentDestination?.id, R.id.converterFragment)
    }



    @Test
    fun login_user_has_token_case() {
        val accessToken = "sample_access_token"
        val expirationTime = System.currentTimeMillis() + 10 * 60 * 1000L
        tokenManager.saveToken(accessToken, expirationTime)

        setUpFragment()

        assertTrue(tokenManager.isTokenValid())
        assertEquals(navController.currentDestination?.id, R.id.converterFragment)
    }

    private fun setUpFragment() {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        loginScenario = launchFragmentInContainer {
            LoginFragment().also { fragment ->

                // In addition to returning a new instance of our Fragment,
                // get a callback whenever the fragment’s view is created
                // or destroyed so that we can set the NavController
                fragment.viewLifecycleOwnerLiveData.observeForever { viewLifecycleOwner ->
                    if (viewLifecycleOwner != null) {
                        // The fragment’s view has just been created
                        navController.setGraph(R.navigation.nav_graph)
                        Navigation.setViewNavController(fragment.requireView(), navController)
                    }
                }
            }
        }
    }
}