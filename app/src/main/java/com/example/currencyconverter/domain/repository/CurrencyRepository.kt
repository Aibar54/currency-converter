package com.example.currencyconverter.domain.repository

import com.example.currencyconverter.domain.model.Currency
import com.example.currencyconverter.util.Resource

interface CurrencyRepository {
    suspend fun getRates(base: String): Resource<List<Currency>>
}