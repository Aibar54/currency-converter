package com.example.currencyconverter.domain.model

data class Currency(
    val name: String,
    val rate: Double,
    val changeForLast24Hour: Double
)