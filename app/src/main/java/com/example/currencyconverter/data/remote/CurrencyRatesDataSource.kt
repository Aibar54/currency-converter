package com.example.currencyconverter.data.remote

import com.example.currencyconverter.data.model.CurrencyResponse
import retrofit2.Response

interface CurrencyRatesDataSource {
    suspend fun getRates(base: String): Response<CurrencyResponse>
}