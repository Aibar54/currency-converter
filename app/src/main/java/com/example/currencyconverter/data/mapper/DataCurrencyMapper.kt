package com.example.currencyconverter.data.mapper

import com.example.currencyconverter.data.model.CurrencyResponse
import com.example.currencyconverter.domain.model.Currency

interface DataCurrencyMapper {

    fun mapToDomain(currencyResponse: CurrencyResponse) : List<Currency>
}