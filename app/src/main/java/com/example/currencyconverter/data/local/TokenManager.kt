package com.example.currencyconverter.data.local

import com.example.currencyconverter.data.local.PrefsProvider.clearToken
import com.example.currencyconverter.data.local.PrefsProvider.getAccessToken
import com.example.currencyconverter.data.local.PrefsProvider.getExpirationTime
import com.example.currencyconverter.data.local.PrefsProvider.setAccessToken
import com.example.currencyconverter.data.local.PrefsProvider.setExpirationTime

class TokenManager {

    private val preferences = PrefsProvider.preferences

    fun saveToken(accessToken: String, expirationTime: Long) {
        preferences.setAccessToken(accessToken)
        preferences.setExpirationTime(System.currentTimeMillis() + expirationTime)
    }

    fun isTokenValid(): Boolean =
        preferences.getAccessToken() != null && System.currentTimeMillis() < preferences.getExpirationTime()

    fun clearToken() = preferences.clearToken()

    companion object {
        private val tokenManager = TokenManager()
        fun getInstance(): TokenManager = tokenManager
    }
}