package com.example.currencyconverter.data.mapper

import com.example.currencyconverter.data.model.CurrencyResponse
import com.example.currencyconverter.domain.model.Currency
import com.example.currencyconverter.util.round
import com.google.gson.JsonObject

class DataCurrencyMapperImpl : DataCurrencyMapper {

    override fun mapToDomain(currencyResponse: CurrencyResponse): List<Currency> {
        val list = mutableListOf<Currency>()

        val it = currencyResponse.data.keySet().iterator()
        val startDateCurrencies: JsonObject = currencyResponse.data.get(it.next()).asJsonObject
        val endDateCurrencies: JsonObject = currencyResponse.data.get(it.next()).asJsonObject

        for (key in startDateCurrencies.keySet()) {
            val startDateCurrency = startDateCurrencies.get(key).asDouble.round(2)
            val endDateCurrency = endDateCurrencies.get(key).asDouble.round(2)
            val change = (endDateCurrency - startDateCurrency).round(2)
            list.add(Currency(key, endDateCurrency, change))
        }

        startDateCurrencies.keySet()
            .map {
                val startDateCurrency = startDateCurrencies.get(it).asDouble.round(2)
                val endDateCurrency = endDateCurrencies.get(it).asDouble.round(2)
                val change = (endDateCurrency - startDateCurrency).round(2)
                Currency(it, endDateCurrency, change)
            }

        return list
    }
}