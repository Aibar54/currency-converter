package com.example.currencyconverter.data.local

import android.content.Context
import android.content.SharedPreferences

const val KEY_ACCESS_TOKEN = "access_token"
const val KEY_EXPIRATION_TIME = "expiration_time"

object PrefsProvider {

    lateinit var preferences: SharedPreferences

    private fun getPreferences(context: Context) : SharedPreferences =
        context.getSharedPreferences("default", 0)

    fun SharedPreferences.setAccessToken(accessToken: String?) {
        edit()
            .putString(KEY_ACCESS_TOKEN, accessToken)
            .apply()
    }

    fun SharedPreferences.getAccessToken() : String? =
        getString(KEY_ACCESS_TOKEN, null)

    fun SharedPreferences.setExpirationTime(expirationTime: Long) {
        edit()
            .putLong(KEY_EXPIRATION_TIME, expirationTime)
            .apply()
    }

    fun SharedPreferences.getExpirationTime() : Long =
        getLong(KEY_EXPIRATION_TIME, 0)

    fun SharedPreferences.clearToken() {
        edit()
            .remove(KEY_ACCESS_TOKEN)
            .remove(KEY_EXPIRATION_TIME)
            .apply()
    }

    fun initialize(appContext: Context) {
        preferences = getPreferences(appContext)
    }
}