package com.example.currencyconverter.data.repository

import com.example.currencyconverter.data.mapper.DataCurrencyMapperImpl
import com.example.currencyconverter.data.remote.CurrencyRatesDataSourceImpl
import com.example.currencyconverter.domain.model.Currency
import com.example.currencyconverter.domain.repository.CurrencyRepository
import com.example.currencyconverter.util.Resource


class CurrencyRepositoryImpl : CurrencyRepository {

    private val dataSource = CurrencyRatesDataSourceImpl()
    private val mapper = DataCurrencyMapperImpl()

    override suspend fun getRates(base: String): Resource<List<Currency>> {
        val response = dataSource.getRates(base)
        val result = response.body()
        if (response.isSuccessful && result != null) {
            return Resource.Success(mapper.mapToDomain(result))
        } else {
            return Resource.Error("Failed connect to the server")
        }
    }

}