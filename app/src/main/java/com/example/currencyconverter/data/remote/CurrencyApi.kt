package com.example.currencyconverter.data.remote

import com.example.currencyconverter.data.model.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {
    @GET("/v1/historical")
    suspend fun getRates(
        @Query("apikey") apiKey: String,
        @Query("date_from") startDate: String,
        @Query("date_to") endDate: String,
        @Query("base_currency") base: String
    ): Response<CurrencyResponse>

    companion object {
        const val API_KEY = "QjjrY3Uq9wpPLggiHXmz6tNAyykCSLymezQXIZxs"
    }
}