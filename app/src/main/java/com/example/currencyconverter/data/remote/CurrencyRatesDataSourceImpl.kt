package com.example.currencyconverter.data.remote

import com.example.currencyconverter.data.model.CurrencyResponse
import com.example.currencyconverter.util.dayBeforeYesterdayDate
import com.example.currencyconverter.util.yesterdayDate
import retrofit2.Response

class CurrencyRatesDataSourceImpl : CurrencyRatesDataSource {
    override suspend fun getRates(base: String): Response<CurrencyResponse> =
        ApiProvider.api.getRates(CurrencyApi.API_KEY, dayBeforeYesterdayDate(), yesterdayDate(), base)
}
