package com.example.currencyconverter.util

import java.text.SimpleDateFormat
import java.util.Calendar
import kotlin.math.roundToInt

fun Double.round(decimals: Int): Double {
    val scale = Math.pow(10.0, decimals.toDouble())
    val number = (this * scale).roundToInt()
    return number / scale
}

fun yesterdayDate(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val cal = Calendar.getInstance()
    cal.add(Calendar.DATE, -1)
    return sdf.format(cal.time)
}

fun dayBeforeYesterdayDate(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val cal = Calendar.getInstance()
    cal.add(Calendar.DATE, -2)
    return sdf.format(cal.time)
}