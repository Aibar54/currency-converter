package com.example.currencyconverter

import android.app.Application
import com.example.currencyconverter.data.local.PrefsProvider

class CurrencyConverterApp : Application() {

    override fun onCreate() {
        super.onCreate()
        PrefsProvider.initialize(this)
    }
}