package com.example.currencyconverter.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.currencyconverter.data.repository.CurrencyRepositoryImpl
import com.example.currencyconverter.domain.model.Currency
import com.example.currencyconverter.domain.repository.CurrencyRepository
import com.example.currencyconverter.util.Resource
import com.example.currencyconverter.util.round
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: CurrencyRepository
) : ViewModel() {

    private val _conversion = MutableLiveData<Event>()
    val conversion: LiveData<Event>
        get() = _conversion

    sealed class Event {
        data class Success(val data: List<Currency>) : Event()
        data class Error(val message: String) : Event()
        object Loading : Event()
    }

    fun convert(amount: String, base: String) {
        if (amount == "") return

        viewModelScope.launch {
            _conversion.value = Event.Loading
            repository.getRates(base).also {
                when (it) {
                    is Resource.Error -> _conversion.value = Event.Error(it.message)
                    is Resource.Success -> {
                        _conversion.value = Event.Success(
                            it.data.map { currency ->
                                Currency(
                                    currency.name,
                                    (currency.rate * amount.toDouble()).round(2),
                                    currency.changeForLast24Hour
                                )
                            })
                    }
                }
            }
        }
    }

    companion object {
        val factory = viewModelFactory {
            initializer {
                MainViewModel(CurrencyRepositoryImpl())
            }
        }
    }
}