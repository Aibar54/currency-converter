package com.example.currencyconverter.presentation.fragment

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.example.currencyconverter.R
import com.example.currencyconverter.data.local.TokenManager
import com.example.currencyconverter.databinding.FragmentConverterBinding
import com.example.currencyconverter.presentation.MainViewModel
import com.example.currencyconverter.presentation.PresentationConstants.TEN_MINUTES_MS
import com.example.currencyconverter.presentation.adapter.CurrencyListAdapter
import com.google.android.material.snackbar.Snackbar

class ConverterFragment : Fragment() {

    private lateinit var binding: FragmentConverterBinding
    private lateinit var adapter: CurrencyListAdapter
    private lateinit var timer: CountDownTimer
    private val viewModel: MainViewModel by viewModels(
        factoryProducer = { MainViewModel.factory }
    )
    private val tokenManager = TokenManager.getInstance()
    private var sessionExpired = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        timer = startTokenExpirationTimer(TEN_MINUTES_MS)
        binding = FragmentConverterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    override fun onStart() {
        super.onStart()
        if(sessionExpired) {
            tokenManager.clearToken()
            showExpiredSessionMessage()
            navigateToLoginScreen()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

    private fun setUpView() = with(binding) {
        adapter = CurrencyListAdapter()
        currencyList.adapter = adapter

        logoutButton.setOnClickListener {
            tokenManager.clearToken()
            navigateToLoginScreen()
        }

        convertButton.setOnClickListener {
            viewModel.convert(
                amount.text.toString(),
                spFromCurrency.selectedItem.toString()
            )
            closeKeyboard()
        }

        viewModel.conversion.observe(viewLifecycleOwner) {
            when (it) {
                MainViewModel.Event.Loading -> progressBar.isVisible = true
                is MainViewModel.Event.Error -> {
                    progressBar.isVisible = false
                    showErrorMessage(it.message)
                }

                is MainViewModel.Event.Success -> {
                    progressBar.isVisible = false
                    adapter.submitList(it.data)
                }
            }
        }
    }

    private fun startTokenExpirationTimer(time: Long): CountDownTimer {
        return object : CountDownTimer(time, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                    tokenManager.clearToken()
                    showExpiredSessionMessage()
                    navigateToLoginScreen()
                } else {
                    sessionExpired = true
                }
            }
        }.start()
    }

    private fun navigateToLoginScreen() {
        findNavController().navigate(R.id.action_converterFragment_to_loginFragment)
    }

    private fun showErrorMessage(message: String) =
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).show()

    private fun showExpiredSessionMessage() =
        Toast.makeText(
            requireActivity(),
            "Session has been expired, please log in again",
            Toast.LENGTH_LONG
        ).show()

    private fun closeKeyboard() {
        val imm =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }
}