package com.example.currencyconverter.presentation

object PresentationConstants {

    const val TEN_MINUTES_MS = 10 * 60 * 1000L
}