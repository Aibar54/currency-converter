package com.example.currencyconverter.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.currencyconverter.R
import com.example.currencyconverter.data.local.TokenManager
import com.example.currencyconverter.databinding.FragmentLoginBinding
import com.example.currencyconverter.presentation.PresentationConstants.TEN_MINUTES_MS

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private val tokenManager = TokenManager.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (tokenManager.isTokenValid()) {
            navigateToConverterScreen()
        }

        binding.loginButton.setOnClickListener {
            val accessToken = "sample_access_token"
            tokenManager.saveToken(accessToken, TEN_MINUTES_MS)
            navigateToConverterScreen()
        }
    }

    private fun navigateToConverterScreen() =
        findNavController().navigate(R.id.action_loginFragment_to_converterFragment)
}