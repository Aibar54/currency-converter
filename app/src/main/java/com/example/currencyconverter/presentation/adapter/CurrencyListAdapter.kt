package com.example.currencyconverter.presentation.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.databinding.CurrencyListItemBinding
import com.example.currencyconverter.domain.model.Currency

class CurrencyListAdapter : ListAdapter<Currency, CurrencyListAdapter.ViewHolder>(asyncCallback) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CurrencyListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ViewHolder(private val binding: CurrencyListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(currency: Currency) = with(binding){
            root.setBackgroundColor(Color.WHITE)
            base.text = currency.name
            rate.text = currency.rate.toString()
            val change24 = currency.changeForLast24Hour

            if(change24 > 0) {
                change.text = "+$change24"
                root.setBackgroundColor(GREEN)
            } else if(change24 < 0) {
                change.text = change24.toString()
                root.setBackgroundColor(RED)
            } else {
                change.text = change24.toString()
            }
        }
    }

    companion object {
        private val GREEN = Color.parseColor("#90EE90")
        private val RED = Color.parseColor("#FFCCCB")

        private val asyncCallback
            get() = object : DiffUtil.ItemCallback<Currency>() {
                override fun areItemsTheSame(oldItem: Currency, newItem: Currency) =
                    oldItem.name == newItem.name

                override fun areContentsTheSame(oldItem: Currency, newItem: Currency) = oldItem == newItem
            }
    }
}